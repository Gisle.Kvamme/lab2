package INF101.lab2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


public class Fridge implements IFridge {

    //FIELDS AND CONSTRUCTORS
    private List<FridgeItem> content = new ArrayList<>();
    private int maxItems = 25;

    public Fridge(){};
    public Fridge(int maxItems){this.maxItems = maxItems;}


    //METHODS
    @Override
    public int nItemsInFridge() {
        return this.content.size();
    }

    @Override
    public int totalSize() {
        return this.maxItems;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() < totalSize())
            this.content.add(item);
        return nItemsInFridge() < totalSize();
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (this.content.contains(item))
            this.content.remove(item);
        else
            throw new NoSuchElementException();
    }

    @Override
    public void emptyFridge() {this.content = new ArrayList<>();}


    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredFood = new ArrayList<>(this.content);

        this.content.removeIf(food -> food.hasExpired());
        expiredFood.removeIf(food -> !food.hasExpired());

        return expiredFood;
    }
}


